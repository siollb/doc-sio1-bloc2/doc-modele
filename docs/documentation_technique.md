# Plateforme X

**[NOTE] À supprimer**  
Ceci ne constitue qu'une ébauche de documentation. Au fil des TP réalisés, vous devez adapter, compléter et apporter toutes les explications nécessaires.
Le schéma réseau doit également être mis à jour régulièrement.

## Schéma réseau

![Schéma réseau plateforme](./schema_reseau_plateforme_init.png)

## Plan d'adressage IP

La plate-forme dispose de deux commutateurs et d’un routeur. Elle est découpée en 2 réseaux logiques 172.2x.110.0/24 et 172.2x.120.0/24 où X représente le numéro de la plateforme.

| | Premier réseau | Deuxième réseau| Réseau d'administration |
| --- | --- | --- | --- |
| Adresse réseau/masque | |  |  |
| Adresse de passerelle | |  |  |  

**[NOTE]**  
À terme, un réseau nécessaire pour l’administration des éléments d’interconnexion aura pour numéro 192.168.91.Nx/26.
Dans un premier temps, nous avons utilisé le Vlan 90 d’administration de CUB.

## Les éléments d'interconnexion

### Les commutateurs

#### Caractéristiques communes des commutateurs

| Type | Description |
| --- | --- |
| Version IOS |  |
| Nom du fichier IOS |  |
| Capacité mémoire Flash |  |
| Capacité NVRAM |  |

#### Adresse IP de management de chaque commutateur

| | Commutateur XA | Commutateur XB|
| --- | --- | --- |
| Adresse IP management | |  |
| Adresse de passerelle | |  |

#### Configuration des Vlan

| Numéro de Vlan |Vlan 110|Vlan 120|Vlan 91|
| --- | --- |---| --- |
| Adresse réseau/masque | | | |
| Adresse de passerelle | | | |
| Ports affectés | | | |

#### Configuration détaillée du commutateur XA

#### Configuration détaillée du commutateur XB

### Le routeur

#### Caractéristiques du routeur

| Type | Description |
| --- | --- |
| Version IOS |  |
| Nom du fichier IOS |  |
| Capacité mémoire Flash |  |
| Capacité NVRAM |  |

#### Éléments de configuration du routeur

|Nom interface | IP/masque|
| --- | --- |
| | |
| | |
| | |
| | |

#### Configuration détaillée du routeur (hors service DHCP)

### Configuration du service DHCP activé sur le routeur

## Récapitulatif de la configuration des ports d’interconnexion

|Routeurs et Commutateurs | Interfaces | Numéro(s) de Vlan | Ports étiquetés avec le protocole 802.1Q (OUI/NON) |
| --- | --- | --- | --- |
| Routeur X| Gb0/0 |  |  |
| Routeur X| Gb0/1 | |  |
| Commutateur de table| Gb0/1 |  |  |
| Commutateur de table| Gb0/2 |  |  |
| Commutateur SW-CUB | Gb0/1 |  |  |

## Récapitulatif des routes relatives à la plateforme

### Table de routage du routeur "RnetCUB"

|Réseau | Masque | Interface| Passerelle |
| --- | --- | --- | --- |
| | | | |
| | | | |

### Table de routage de "FW-CUB-CORSICA"

|Réseau | Masque | Interface| Passerelle |
| --- | --- | --- | --- |
| | | | |
| | | | |

### Table de routage de "FW-LAN"

|Réseau | Masque | Interface| Passerelle |
| --- | --- | --- | --- |
| | | | |
| | | | |
