# Documentation modèle

**[NOTE] À supprimer**  
Ceci constitue un exemple de la documentation que vous pouvez réaliser mais n'hésitez pas à vous en détacher.
Modifiez ce fichier (y compris le titre) pour expliquer le contexte et la mise en oeuvre de votre plateforme, comme vous le faites en AP.

## Présentation de la réalisation professionnelle

La documentation technique de la réalisation professionnelle est dans un [fichier à part](./documentation_technique.md).

### Description du besoin

La société fictive CUB est une entreprise spécialisée dans l’incubation de startups partageant les mêmes valeurs de solidarité et de développement durable. Elle met à disposition de ses clients un ensemble de solutions techniques d’accès dans un millier de salles de réunion situées dans une quarantaine de villes différentes.

Nous sommes des techniciens de la startup AJASIO dont les serveurs sont hébergés par CUB.  
AJASIO gère notamment l’infrastructure réseau du lycée Laetitia Bonaparte.  
Pour lui permettre d’intervenir et de déployer plus rapidement les solutions, cette dernière a besoin de disposer au sein de la structure de CUB de deux nouveaux réseaux logiques à des fins de test. Chaque nouveau réseau représente une section du lycée devant se partager des éléments physiques commun (comme un commutateur ou un point d’accès), comme c’est le cas actuellement pour la plupart des sections.

L’objectif final est de disposer d’une plateforme de test similaire à la maquette Packet Tracer présentée ci-dessous sachant que le réseau de CUB n'est évidemment pas de notre responsabilité mis à part un certain nombre de serveurs virtuels mis à notre disposition.

![Maquette Packet Tracer](./maquette_pt.png)

### Ressources matérielles

| Matériel | Quantité | Commentaires/Description |
| --- | --- | --- |
| Routeur Cisco 4221 | 1 |  |
| Commutateurs Cisco C1000 | 2 |  |
| Postes de travail | 8 |  |
| Câbles console avec adaptateurs | 2 |  |
| Câbles RJ45 | 13 |  |

### Serveurs virtuels

## Phases de réalisation

## Bilan et compétences acquises
